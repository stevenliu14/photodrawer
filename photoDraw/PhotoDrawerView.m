//
//  PhotoDrawerView.m
//  photoDraw
//
//  Created by Steven Liu on 2016-05-25.
//  Copyright © 2016 Steven Liu. All rights reserved.
//

#import "PhotoDrawerView.h"
#import "PhotoDrawerNavigationBar.h"
#import "PhotoDrawerToolBar.h"

@interface PhotoDrawerView ()

@end

@implementation PhotoDrawerView

#define HEIGHT_OF_NAV 60
#define HEIGHT_OF_TOOLBAR 44
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {

        self.backgroundColor = [UIColor whiteColor];
        
        
        [self initNavbar];
        [self initToolBar];
        [self initImageView];
    }
    
    return self;
}


- (void)initNavbar
{
    _navBar = [[PhotoDrawerNavigationBar alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, HEIGHT_OF_NAV)];
    [self addSubview:_navBar];
}

- (void)initToolBar
{
    _toolBar = [[PhotoDrawerToolBar alloc] initWithFrame:CGRectMake(self.bounds.origin.x,
                                                           self.bounds.size.height-HEIGHT_OF_TOOLBAR,
                                                           self.bounds.size.width,
                                                           HEIGHT_OF_TOOLBAR)];
    
    [self addSubview:_toolBar];
}

- (void)initImageView
{
    CGRect imageFrame = CGRectMake(0,
                                   HEIGHT_OF_NAV,
                                   self.bounds.size.width,
                                   (self.bounds.size.height-HEIGHT_OF_NAV-HEIGHT_OF_TOOLBAR));
    
    _imageView = [[UIImageView alloc] initWithFrame:imageFrame];
    _drawView = [[UIImageView alloc] initWithFrame:imageFrame];
    _persistDrawView = [[UIImageView alloc] initWithFrame:imageFrame];
    
    [_imageView setContentMode:UIViewContentModeScaleAspectFit];

    [self addSubview:_imageView];
    [self addSubview:_persistDrawView];
    [self addSubview:_drawView];

}


@end
