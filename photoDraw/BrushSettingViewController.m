//
//  BrushSettingViewController.m
//  photoDraw
//
//  Created by Steven Liu on 2016-07-08.
//  Copyright © 2016 Steven Liu. All rights reserved.
//

#import "BrushSettingViewController.h"
#import "PhotoDrawerNavigationBar.h"

@interface BrushSettingViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *redValueTextField;
@property (weak, nonatomic) IBOutlet UITextField *greenValueTextField;
@property (weak, nonatomic) IBOutlet UITextField *blueValueTextField;
@property (weak, nonatomic) IBOutlet UITextField *opacityValueTextField;
@property (weak, nonatomic) IBOutlet UITextField *brushSizeValueTextField;

@property (weak, nonatomic) IBOutlet UISlider *redValueSlider;
@property (weak, nonatomic) IBOutlet UISlider *greenValueSlider;
@property (weak, nonatomic) IBOutlet UISlider *blueValueSlider;
@property (weak, nonatomic) IBOutlet UISlider *opacityValueSlider;
@property (weak, nonatomic) IBOutlet UISlider *brushSizeValueSlider;

@property (weak, nonatomic) IBOutlet UIImageView *brushPreviewImageView;

@property (strong, nonatomic) UINavigationBar *navBar;

@end

@implementation BrushSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpNavBar];
    [self setupValues];
    [self drawPreviewImage];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    [self.brush changeColor:[self.redValueTextField.text floatValue]/255.0 green:[self.greenValueTextField.text floatValue]/255.0 blue:[self.blueValueTextField.text floatValue]/255.0];
    self.brush.opacity = [self.opacityValueTextField.text floatValue]/100.0;
    self.brush.brushSize = [self.brushSizeValueTextField.text floatValue];
}

- (void)dismissSetting:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Setup Methods
- (void)setupValues {
    self.brush.red *= 255.0;
    self.brush.green *= 255.0;
    self.brush.blue *= 255.0;
    self.brush.opacity *= 100.0;
    
    self.redValueSlider.value = self.brush.red;
    self.greenValueSlider.value = self.brush.green;
    self.blueValueSlider.value = self.brush.blue;
    self.opacityValueSlider.value = self.brush.opacity;
    self.brushSizeValueSlider.value = self.brush.brushSize;

    self.redValueTextField.text = [NSString stringWithFormat:@"%d", (int)self.brush.red];
    self.greenValueTextField.text = [NSString stringWithFormat:@"%d", (int)self.brush.green];
    self.blueValueTextField.text = [NSString stringWithFormat:@"%d", (int)self.brush.blue];
    self.opacityValueTextField.text = [NSString stringWithFormat:@"%d", (int)self.brush.opacity];
    self.brushSizeValueTextField.text = [NSString stringWithFormat:@"%d", (int)self.brush.brushSize];
}

#define HEIGHT_OF_NAV 60
- (void)setUpNavBar
{
    self.navBar = [[PhotoDrawerNavigationBar alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, HEIGHT_OF_NAV)];
    UINavigationItem *navItem = [[UINavigationItem alloc] initWithTitle:@"Settings"];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissSetting:)];
    [navItem setRightBarButtonItem:doneButton];
    [self.navBar setItems:@[navItem]];
    [self.view addSubview:self.navBar];
}


#pragma mark - Slider Delegates
- (IBAction)redValueSliderChanged:(id)sender {
    self.redValueTextField.text = [NSString stringWithFormat:@"%d", (int)self.redValueSlider.value];
    [self drawPreviewImage];
}

- (IBAction)greenValueSliderChanged:(id)sender {
    self.greenValueTextField.text = [NSString stringWithFormat:@"%d", (int)self.greenValueSlider.value];
    [self drawPreviewImage];
}

- (IBAction)blueValueSliderChanged:(id)sender {
    self.blueValueTextField.text = [NSString stringWithFormat:@"%d", (int)self.blueValueSlider.value];
    [self drawPreviewImage];
}

- (IBAction)opacityValueSliderChanged:(id)sender {
    self.opacityValueTextField.text = [NSString stringWithFormat:@"%d", (int)self.opacityValueSlider.value];
    [self drawPreviewImage];
}

- (IBAction)brushSizeValueChanged:(id)sender {
    self.brushSizeValueTextField.text = [NSString stringWithFormat:@"%d", (int)self.brushSizeValueSlider.value];
    [self drawPreviewImage];
}

#pragma mark - TextField Delegates
- (IBAction)backgroundViewtapped:(id)sender {
    [self.view endEditing:YES];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if([textField.text isEqual:@"0"]) {
        textField.text = @"";
    }

    return YES;
}

#define MAX_TEXTFIELD_LENGTH 3
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= MAX_TEXTFIELD_LENGTH;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (textField == self.redValueTextField) {
        self.redValueSlider.value = [self.redValueTextField.text integerValue];
    }
    else if (textField == self.greenValueTextField) {
        self.greenValueSlider.value = [self.greenValueTextField.text integerValue];
    }
    else if (textField == self.blueValueTextField) {
        self.blueValueSlider.value = [self.blueValueTextField.text integerValue];
    }
    else if (textField == self.opacityValueTextField) {
        self.opacityValueSlider.value = [self.opacityValueTextField.text integerValue];
    }
    else {
        self.brushSizeValueSlider.value = [self.brushSizeValueTextField.text integerValue];
    }
    
    [self drawPreviewImage];
    
    if ([textField.text isEqualToString:@""]) {
        textField.text = @"0";
    }
    
    if ([textField.text integerValue] > 255) {
        textField.text = @"255";
    }
    
    return YES;
}

#pragma mark - UIMenuController Delegates
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}


#pragma mark - Draw Method
- (void)drawPreviewImage
{
    UIGraphicsBeginImageContextWithOptions(self.brushPreviewImageView.bounds.size, NO, 0.0);
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), self.brush.lineCap);
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), self.brushSizeValueSlider.value);
    CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), self.redValueSlider.value/255.0, self.greenValueSlider.value/255.0, self.blueValueSlider.value/255.0, self.opacityValueSlider.value/100.0);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), self.brush.blendMode);
    CGContextMoveToPoint(UIGraphicsGetCurrentContext(), self.brushPreviewImageView.bounds.size.width/2, self.brushPreviewImageView.bounds.size.height/2);
    CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), self.brushPreviewImageView.bounds.size.width/2, self.brushPreviewImageView.bounds.size.height/2);
    CGContextStrokePath(UIGraphicsGetCurrentContext());
    self.brushPreviewImageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
}

@end
