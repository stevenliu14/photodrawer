//
//  PhotoDrawerView.h
//  photoDraw
//
//  Created by Steven Liu on 2016-05-25.
//  Copyright © 2016 Steven Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoDrawerView : UIView

@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UIImageView *drawView;
@property (strong, nonatomic) UIImageView *persistDrawView;
@property (strong, nonatomic) UIToolbar *toolBar;
@property (strong, nonatomic) UINavigationBar *navBar;
@end
