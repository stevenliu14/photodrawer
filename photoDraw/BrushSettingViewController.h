//
//  BrushSettingViewController.h
//  photoDraw
//
//  Created by Steven Liu on 2016-07-08.
//  Copyright © 2016 Steven Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Brush.h"

@interface BrushSettingViewController : UIViewController

@property (strong, nonatomic) Brush *brush;
@end
