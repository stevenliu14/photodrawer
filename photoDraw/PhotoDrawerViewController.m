//
//  PhotoDrawerViewController.m
//  photoDraw
//
//  Created by Steven Liu on 2016-05-25.
//  Copyright © 2016 Steven Liu. All rights reserved.
//

#import "PhotoDrawerViewController.h"
#import "PhotoDrawerView.h"
#import "BrushSettingViewController.h"
#import "Brush.h"

@interface PhotoDrawerViewController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (strong, nonatomic) PhotoDrawerView *photoView;
@property (strong, nonatomic) Brush *myBrush;
@property (strong, nonatomic) Brush *eraser;
@property (strong, nonatomic) Brush *pen;

@property (assign, nonatomic) CGPoint lastPoint;
@property (assign, nonatomic) BOOL mouseSwipe;

@end

@implementation PhotoDrawerViewController


-(void)loadView
{
    self.photoView =[[PhotoDrawerView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.view = self.photoView;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                                                                target:self
                                                                                action:@selector(shareImage)];


    UIBarButtonItem *clearButton = [[UIBarButtonItem alloc] initWithTitle:@"Clear"
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self
                                                                   action:@selector(clearSketch)];
    
    
    UINavigationItem *navItem = [[UINavigationItem alloc] initWithTitle:[NSString stringWithFormat:@"Version %@ Build %@", [self appVersion], [self build]]];
    [navItem setRightBarButtonItem:saveButton];
    [navItem setLeftBarButtonItem:clearButton];
    [self.photoView.navBar setItems:@[navItem]];
    
    self.pen = [[Brush alloc] init];
    self.eraser = [[Brush alloc] init];
    self.myBrush = self.pen;
    [self.eraser eraserSetting];
}

#pragma mark - Toolbar buttons
- (void)settingsBtn
{
    BrushSettingViewController *settingVC = [[BrushSettingViewController alloc] init];
    settingVC.brush = self.pen;
    [self presentViewController:settingVC animated:YES completion:nil];
}

- (void)penBtn
{
    self.myBrush = self.pen;
}

- (void)eraserBtn
{
    self.myBrush = self.eraser;
}

- (void)clearSketch
{
    if (!self.photoView.persistDrawView.image) {
        self.photoView.imageView.image = nil;
    }
    self.photoView.persistDrawView.image = nil;
    
}

- (void)takePic:(id)sender
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;

    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertController *photoOptions = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"Take A Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:imagePicker animated:YES completion:nil];
        }];
        UIAlertAction *photoLibraryAction = [UIAlertAction actionWithTitle:@"Choose A Photo From Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:imagePicker animated:YES completion:nil];
        }];
        
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];

        [photoOptions addAction:cameraAction];
        [photoOptions addAction:photoLibraryAction];
        [photoOptions addAction:defaultAction];
        [self presentViewController:photoOptions animated:YES completion:nil];
    } else {
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:imagePicker animated:YES completion:nil];

    }
    
}

- (void)shareImage
{
    UIImage *saveImage = [self getCurrentImageFromScreen];
    
    UIActivityViewController *activityController = [self setupWithActivityItems:@[saveImage]];
    
    [self presentViewController:activityController animated:YES completion:nil];
}

#pragma mark - Camera Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage *imagePicked = info[UIImagePickerControllerOriginalImage];
    imagePicked = [self scaleImage:imagePicked toSize:self.photoView.imageView.bounds.size];
    CGPoint center;
    center.x = (self.photoView.drawView.bounds.size.width/2.0)-(imagePicked.size.width/2.0);
    center.y = (self.photoView.drawView.bounds.size.height/2.0)-(imagePicked.size.height/2.0);
    
    UIGraphicsBeginImageContextWithOptions(self.photoView.drawView.bounds.size, NO, 0.0);
    [imagePicked drawInRect:CGRectMake(center.x, center.y, imagePicked.size.width, imagePicked.size.height)];
    self.photoView.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIGraphicsBeginImageContextWithOptions(self.photoView.drawView.bounds.size, NO, 0.0);
    [self.photoView.persistDrawView.image drawInRect:self.photoView.persistDrawView.bounds];
    [imagePicked drawInRect:CGRectMake(center.x, center.y, imagePicked.size.width, imagePicked.size.height)];
    self.photoView.persistDrawView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIResponder Methods
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    self.mouseSwipe = NO;
    self.lastPoint = [[touches anyObject] locationInView:self.photoView.drawView];
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    self.mouseSwipe = YES;
    CGPoint currentPoint = [[touches anyObject] locationInView:self.photoView.drawView];
    
    CGFloat tempOpacity = self.myBrush.opacity;
    self.myBrush.opacity = 1.0;
    
    
    self.photoView.drawView.image = [self drawFromPoint:self.lastPoint
                                                toPoint:currentPoint
                                                 inView:self.photoView.drawView
                                              withBrush:self.myBrush];
    self.myBrush.opacity = tempOpacity;
    
    [self.photoView.drawView setAlpha:self.myBrush.opacity];
    
    self.lastPoint = currentPoint;
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    if(!self.mouseSwipe) {
    
        self.photoView.drawView.image = [self drawFromPoint:self.lastPoint
                                                    toPoint:self.lastPoint
                                                     inView:self.photoView.drawView
                                                  withBrush:self.myBrush];
    }
    
    UIGraphicsBeginImageContextWithOptions(self.photoView.drawView.bounds.size, NO, 0.0);
    [self.photoView.persistDrawView.image drawInRect:self.photoView.persistDrawView.bounds blendMode:kCGBlendModeNormal alpha:1.0];
    [self.photoView.drawView.image drawInRect:self.photoView.drawView.bounds blendMode:kCGBlendModeNormal alpha:self.myBrush.opacity];
    
    self.photoView.persistDrawView.image = UIGraphicsGetImageFromCurrentImageContext();
    self.photoView.drawView.image = nil;
    UIGraphicsEndImageContext();
}


#pragma mark - Helper Methods
- (UIImage*)scaleImage:(UIImage*)image toSize:(CGSize)newSize {
    CGSize scaledSize = newSize;
    float scaleFactor = image.size.width / image.size.height;

    if(image.size.width > image.size.height) {
        scaledSize.width = newSize.width;
        scaledSize.height = newSize.width / scaleFactor;
    }
    else {
        scaledSize.height = newSize.height;
        scaledSize.width = scaleFactor * newSize.height;
    }
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(floor(scaledSize.width), floor(scaledSize.height)), NO, 0.0);
    [image drawInRect:CGRectMake( 0.0, 0.0, scaledSize.width, scaledSize.height )];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return scaledImage;
}

- (UIImage *)drawFromPoint:(CGPoint)fromPoint toPoint:(CGPoint)currentPoint inView:(UIImageView *)view withBrush:(Brush *)brush
{
    
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, 0.0);
    [view.image drawInRect:view.bounds];
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, fromPoint.x, fromPoint.y);
    CGPathAddLineToPoint(path, NULL, currentPoint.x, currentPoint.y);
    CGContextAddPath(UIGraphicsGetCurrentContext(), path);
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), brush.lineCap);
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), brush.brushSize);
    CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), brush.red, brush.green, brush.blue, brush.opacity);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), brush.blendMode);
    CGContextStrokePath(UIGraphicsGetCurrentContext());
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    CGPathRelease(path);
    UIGraphicsEndImageContext();

    return image;
}

- (UIImage*)getCurrentImageFromScreen
{
    UIGraphicsBeginImageContextWithOptions(self.photoView.persistDrawView.bounds.size, NO, 0.0);
    [self.photoView.imageView.image drawInRect:self.photoView.imageView.bounds];
    [self.photoView.persistDrawView.image drawInRect:self.photoView.persistDrawView.bounds];
    UIImage *saveImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return saveImage;
}

- (UIActivityViewController *)setupWithActivityItems:(NSArray *)imagesToShare
{
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:imagesToShare
                                      applicationActivities:nil];
    // access the completion handler
    activityController.completionWithItemsHandler = ^(NSString *activityType,
                                                      BOOL completed,
                                                      NSArray *returnedItems,
                                                      NSError *error){
        // react to the completion
        if (completed) {
            
            // if user saved image to camera roll
            if ([activityType isEqualToString:UIActivityTypeSaveToCameraRoll]) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Success" message:@"Image was successfully saved into the photo album" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
            // user shared an item
            NSLog(@"We used activity type%@", activityType);
            
        } else {
            
            // user cancelled
            NSLog(@"We didn't want to share anything after all.");
        }
        
        if (error) {
            NSLog(@"An Error occured: %@, %@", error.localizedDescription, error.localizedFailureReason);
        }
    };
    return activityController;
}

- (NSString *) appVersion
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
}

- (NSString *) build
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
}


@end
