//
//  PhotoDrawerToolBar.m
//  photoDraw
//
//  Created by Steven Liu on 2016-09-07.
//  Copyright © 2016 Steven Liu. All rights reserved.
//

#import "PhotoDrawerToolBar.h"

@implementation PhotoDrawerToolBar

#define kRed (11.0/255.0)
#define kGreen (211.0/255.0)
#define kBlue (24.0/255.0)

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        UIBarButtonItem *flexspace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                   target:nil
                                                                                   action:nil];
        
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"

        
        UIBarButtonItem *eraserButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"eraser"]
                                                                         style:UIBarButtonItemStylePlain
                                                                        target:nil
                                                                        action:@selector(eraserBtn)];
        
        UIBarButtonItem *penButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"pen"]
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:nil
                                                                     action:@selector(penBtn)];
        
        UIBarButtonItem *cameraButton  = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCamera
                                                                                       target:nil
                                                                                       action:@selector(takePic:)];
        
        
        UIBarButtonItem *colorButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"eyeDropper"]
                                                                        style:UIBarButtonItemStylePlain
                                                                       target:nil
                                                                       action:@selector(settingsBtn)];
#pragma clang diagnostic pop
        
        [self setItems:@[flexspace, eraserButton, flexspace, penButton, flexspace, cameraButton, flexspace, colorButton, flexspace]];
        
        
        
        self.barTintColor = [UIColor colorWithRed:kRed
                                                green:kGreen
                                                 blue:kBlue
                                                alpha:1];
        self.tintColor = [UIColor whiteColor];

    }
    return self;
}

@end
