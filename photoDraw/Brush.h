//
//  Brush.h
//  photoDraw
//
//  Created by Steven Liu on 2016-07-05.
//  Copyright © 2016 Steven Liu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Brush : NSObject

@property (assign, nonatomic) CGFloat brushSize;
@property (assign, nonatomic) CGFloat red;
@property (assign, nonatomic) CGFloat green;
@property (assign, nonatomic) CGFloat blue;
@property (assign, nonatomic) CGFloat opacity;
@property (assign, nonatomic) CGBlendMode blendMode;
@property (assign, nonatomic) CGLineCap lineCap;


- (void)eraserSetting;
- (void)changeColor:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue;


@end
