//
//  PhotoDrawerNavigationBar.h
//  photoDraw
//
//  Created by Steven Liu on 2016-08-08.
//  Copyright © 2016 Steven Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoDrawerNavigationBar : UINavigationBar

@end
