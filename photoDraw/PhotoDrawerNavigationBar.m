//
//  PhotoDrawerNavigationBar.m
//  photoDraw
//
//  Created by Steven Liu on 2016-08-08.
//  Copyright © 2016 Steven Liu. All rights reserved.
//

#import "PhotoDrawerNavigationBar.h"

@implementation PhotoDrawerNavigationBar

#define kRed (11.0/255.0)
#define kGreen (211.0/255.0)
#define kBlue (24.0/255.0)

- (instancetype)initWithFrame:(CGRect)frame
{
    
    if (self = [super initWithFrame:frame]) {
        // Use a round about way to remove the nav bar shadow
        UIColor *navBarColor = [UIColor colorWithRed:kRed
                                               green:kGreen
                                                blue:kBlue
                                               alpha:1];
        // Need to create a background image
        [self setBackgroundImage:[self imageFromColor:navBarColor withRect:frame]
                  forBarPosition:UIBarPositionAny
                      barMetrics:UIBarMetricsDefault];
        
        // Then set the shadow to a new image
        [self setShadowImage:[[UIImage alloc] init]];
        
        self.tintColor = [UIColor whiteColor];
    }
    
    return self;

}



- (UIImage *)imageFromColor:(UIColor *)color withRect:(CGRect)rect
{
    UIGraphicsBeginImageContext(rect.size);
    CGContextSetFillColorWithColor(UIGraphicsGetCurrentContext(), [color CGColor]);
    CGContextFillRect(UIGraphicsGetCurrentContext(), rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end
