//
//  Brush.m
//  photoDraw
//
//  Created by Steven Liu on 2016-07-05.
//  Copyright © 2016 Steven Liu. All rights reserved.
//

#import "Brush.h"

@implementation Brush

-(instancetype)init
{
    if(self = [super init]) {
        _red = 0.0;
        _green = 0.0;
        _blue = 0.0;
        _brushSize = 10.0;
        _opacity = 1.0;
        _blendMode = kCGBlendModeNormal;
        _lineCap = kCGLineCapRound;
    }
    return self;
}


- (void)changeColor:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue
{
    self.red = red;
    self.green = green;
    self.blue = blue;
}

- (void)eraserSetting
{
    self.red = 255.0/255.0;
    self.green = 255.0/255.0;
    self.blue = 255.0/255.0;
    self.brushSize = 100.0;
    self.opacity = 1.0;
    self.blendMode = kCGBlendModeNormal;
    self.lineCap = kCGLineCapRound;
}


@end
